package com.example.dimysik.tz_woxapp.api

import com.example.dimysik.tz_woxapp.model.geocode.Example
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface GeoCodeInterface{

    @GET("maps/api/geocode/json?")
    fun getLocation(@Query("address") address: String/*,
                    @Query("key") googleMapKey: String*/):io.reactivex.Single<Example>

    //travel_mode = walking
    @GET("maps/api/directions/json")
    fun getRoute(@Query("origin") origin: String,
                 @Query("destination") destination: String,
                 @Query("waypoints") waypoints:String?,
                 @Query("travel_mode") travelMode: String):io.reactivex.Single<com.example.dimysik.tz_woxapp.model.direction.Example>

    companion object Factory{
        fun create():GeoCodeInterface {
            val gson: Gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("https://maps.googleapis.com/")
                    .build()

            return retrofit.create(GeoCodeInterface::class.java)
        }
    }
}