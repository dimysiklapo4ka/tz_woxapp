package com.example.dimysik.tz_woxapp.model.realmModel

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class DirectionModel(
        /*@PrimaryKey var id: Long = 0,*/
        var placesLat : RealmList<Double> = RealmList(),
        var placesLng : RealmList<Double> = RealmList(),
        var placesWayPoint: RealmList<String> = RealmList(),
        var placesName: RealmList<String?> = RealmList()): RealmObject(){}