package com.example.dimysik.tz_woxapp.model.direction

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.example.dimysik.tz_woxapp.model.geocode.Southwest
import com.example.dimysik.tz_woxapp.model.geocode.Northeast

data class Example (

    @SerializedName("routes")
    @Expose
    val routes: List<com.example.dimysik.tz_woxapp.model.direction.Route>?,
    @SerializedName("status")
    @Expose
    val status: String?

)

data class Bounds (

    @SerializedName("northeast")
    @Expose
    val northeast: com.example.dimysik.tz_woxapp.model.direction.Northeast?,
    @SerializedName("southwest")
    @Expose
    val southwest: com.example.dimysik.tz_woxapp.model.direction.Southwest?

    )

data class Northeast (

    @SerializedName("lat")
    @Expose
    val lat: Double?,
    @SerializedName("lng")
    @Expose
    val lng: Double?

    )

data class OverviewPolyline (

    @SerializedName("points")
    @Expose
    val points: String?

    )

data class Route (

    @SerializedName("bounds")
    @Expose
    val bounds: Bounds?,
    @SerializedName("copyrights")
    @Expose
    val copyrights: String?,
    @SerializedName("legs")
    @Expose
    val legs: List<Any>?,
    @SerializedName("overview_polyline")
    @Expose
    val overviewPolyline: OverviewPolyline?,
    @SerializedName("summary")
    @Expose
    val summary: String?,
    @SerializedName("warnings")
    @Expose
    val warnings: List<Any>?,
    @SerializedName("waypoint_order")
    @Expose
    val waypointOrder: List<Any>?

    )

data class Southwest (

    @SerializedName("lat")
    @Expose
    val lat: Double?,
    @SerializedName("lng")
    @Expose
    val lng: Double?

    )