package com.example.dimysik.tz_woxapp.model.geocode

import com.google.gson.annotations.SerializedName


data class AddressComponent (
    @SerializedName("long_name")
    val longName: String?,
    @SerializedName("short_name")
    val shortName: String?,
    @SerializedName("types")
    val types: MutableList<String>?
)

data class Example (
    @SerializedName("results")
    val results: MutableList<Result>?,
    @SerializedName("status")
    val status: String?
)

data class Geometry (
    @SerializedName("location")
    val location: Location?,
    @SerializedName("location_type")
    val locationType: String?,
    @SerializedName("viewport")
    val viewport: Viewport?
)

data class Location (
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lng")
    val lng: Double?
)

data class Northeast (
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lng")
    val lng: Double?
)

data class Result (
    @SerializedName("address_components")
    val addressComponents: MutableList<AddressComponent>?,
    @SerializedName("formatted_address")
    val formattedAddress: String?,
    @SerializedName("geometry")
    val geometry: Geometry?,
    @SerializedName("place_id")
    val placeId: String?,
    @SerializedName("types")
    val types: MutableList<String>?
)


data class Southwest (
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lng")
    val lng: Double?
)


data class Viewport (
    @SerializedName("northeast")
    val northeast: Northeast?,
    @SerializedName("southwest")
    val southwest: Southwest?
)
