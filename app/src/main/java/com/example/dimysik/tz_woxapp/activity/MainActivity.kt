package com.example.dimysik.tz_woxapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.MenuItem
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.example.dimysik.tz_woxapp.R
import com.example.dimysik.tz_woxapp.fragment.map.MyMapFragment
import com.example.dimysik.tz_woxapp.fragment.wayList.WaysListFragment

const val MAP_FRAGMENT = "MapFragment"
const val LIST_FRAGMENT = "WaysListFragment"

class MainActivity : AppCompatActivity() {

    @BindView(R.id.bnv_enter_the_way)
    lateinit var bottomNavigationView:BottomNavigationView

    private val mapFragment: MyMapFragment = MyMapFragment()
    private val listFragment: WaysListFragment = WaysListFragment()
    private val fragmentManager:FragmentManager = supportFragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private val clicked:Clicked = Clicked()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        init()
        replaceFragment(mapFragment, MAP_FRAGMENT)
    }

    private fun init(){
        bottomNavigationView.setOnNavigationItemSelectedListener(clicked)
    }

    private fun replaceFragment(fragment:Fragment, fragmentTag: String) {
        fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container,fragment,fragmentTag)
                .commit()
    }

    private inner class Clicked : View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when(item.itemId){
                R.id.action_map -> replaceFragment(mapFragment, MAP_FRAGMENT)
                R.id.action_list -> replaceFragment(listFragment, LIST_FRAGMENT)
            }
            return true
        }

        override fun onClick(view: View?) {
            when(view!!.id){

            }
        }

    }
}
