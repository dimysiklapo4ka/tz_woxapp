package com.example.dimysik.tz_woxapp.fragment.wayList.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.dimysik.tz_woxapp.R
import com.example.dimysik.tz_woxapp.model.realmModel.DirectionModel

class WaysListAdapter(context: Context?,rootList: MutableList<DirectionModel>): RecyclerView.Adapter<WaysListViewHolder>() {

    private val list: MutableList<DirectionModel> = rootList
    private val context: Context? = context
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): WaysListViewHolder {
        return WaysListViewHolder(inflater.inflate(R.layout.ways_item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: WaysListViewHolder?, position: Int) {
        holder!!.bind(list[position])
    }

}

class WaysListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val mStartWay: TextView = itemView.findViewById<View>(R.id.start_way) as TextView
    private val mEndWay: TextView = itemView.findViewById<View>(R.id.end_way) as TextView
    private val mOnePoint: TextView = itemView.findViewById<View>(R.id.one_point) as TextView
    private val mTwoPoint: TextView = itemView.findViewById<View>(R.id.two_point) as TextView
    private val mThreePoint: TextView = itemView.findViewById<View>(R.id.three_point) as TextView
    private val mFourPoint: TextView = itemView.findViewById<View>(R.id.four_point) as TextView
    private val mFivePoint: TextView = itemView.findViewById<View>(R.id.five_point) as TextView

    private val clicked: Clicked = Clicked()

    init {
        itemView.setOnClickListener(clicked)
    }

    fun bind(directionModel: DirectionModel){
        for (i in 1 until directionModel.placesName.size) {
            when(i) {
                1 -> setItemtext(mStartWay, directionModel.placesName[0])
                2 -> setItemtext(mEndWay, directionModel.placesName[1])
                3 -> setItemtext(mOnePoint, directionModel.placesName[2])
                4 -> setItemtext(mTwoPoint, directionModel.placesName[3])
                5 -> setItemtext(mThreePoint, directionModel.placesName[4])
                6 -> setItemtext(mFourPoint, directionModel.placesName[5])
                7 -> setItemtext(mFivePoint, directionModel.placesName[6])
            }
        }
    }

    private fun setItemtext(view: TextView, string: String?){
        view.text = string
        view.visibility = View.VISIBLE
    }

    private inner class Clicked: View.OnClickListener{
        override fun onClick(v: View?) {

        }

    }
}