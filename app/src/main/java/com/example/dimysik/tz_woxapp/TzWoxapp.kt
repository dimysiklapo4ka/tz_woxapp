package com.example.dimysik.tz_woxapp

import android.app.Application
import io.realm.Realm

class TzWoxapp: Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}