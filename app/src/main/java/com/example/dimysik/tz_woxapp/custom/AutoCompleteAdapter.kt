package com.example.dimysik.tz_woxapp.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.dimysik.tz_woxapp.model.geocode.Result

class AutoCompleteAdapter(context: Context, resource: Int, list: MutableList<Result>?):
        ArrayAdapter<Result>(context, resource, list) {
    private var resource: Int
    private var list: List<Result>?

    init {
        this.resource = resource
        this.list = list
    }
    
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return createViewFromResource(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return createViewFromResource(position, convertView, parent)
    }

    private fun createViewFromResource(position: Int, convertView: View?, parent: ViewGroup?): View{
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(resource, parent, false) as TextView
        if (list != null)view.text = list!![position].formattedAddress
        return view
    }

    override fun getCount(): Int {
        return if (list!= null) list!!.size
        else 0
    }


}