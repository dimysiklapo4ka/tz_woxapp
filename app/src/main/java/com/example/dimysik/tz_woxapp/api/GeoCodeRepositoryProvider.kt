package com.example.dimysik.tz_woxapp.api

object GeoCodeRepositoryProvider{
    fun provideGeoCodeRepository():GeoCodeRepository{
        return GeoCodeRepository(GeoCodeInterface.create())
    }
}