package com.example.dimysik.tz_woxapp.custom

import android.R
import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import com.example.dimysik.tz_woxapp.api.GeoCodeRepository
import com.example.dimysik.tz_woxapp.api.GeoCodeRepositoryProvider
import com.example.dimysik.tz_woxapp.model.geocode.Example
import com.example.dimysik.tz_woxapp.model.geocode.Result
import com.google.android.gms.maps.model.LatLng
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MyAutoCompleteTextView(context: Context?, attrs: AttributeSet?) :
        AutoCompleteTextView(context, attrs){

    private val TAG = MyAutoCompleteTextView::class.java.simpleName

    private val geoCode: GeoCodeRepository = GeoCodeRepositoryProvider.provideGeoCodeRepository()
    private val compositeDisposable : CompositeDisposable = CompositeDisposable()

    override fun addTextChangedListener(watcher: TextWatcher?) {
        if (watcher == null){
            super.addTextChangedListener(MyTextWatcher(this))
        }else {
            super.addTextChangedListener(watcher)
        }
    }

    inner class MyTextWatcher(val view: View) : TextWatcher{
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s.toString().length >= 3){
                compositeDisposable.add(geoCode.getLocation(s.toString())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({it -> getAddress(it)},{it -> getThrowable(it)}))
            }
        }
    }

    private fun getAddress(example: Example){
        Log.e(TAG, "STATUS -> " + example.status+ " :: "
                + example.results.toString())
        when(example.status){
            "OK" -> {
                val adapter = AutoCompleteAdapter(context!!, R.layout.simple_dropdown_item_1line, example.results)
                this.setAdapter(adapter)
                this.showDropDown()
            }
        }
    }

    private fun getThrowable(t: Throwable){
        Log.e(TAG, t.message, t)
    }

}