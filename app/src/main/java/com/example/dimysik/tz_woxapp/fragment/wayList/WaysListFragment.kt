package com.example.dimysik.tz_woxapp.fragment.wayList

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.example.dimysik.tz_woxapp.R
import com.example.dimysik.tz_woxapp.fragment.wayList.adapter.WaysListAdapter
import com.example.dimysik.tz_woxapp.model.realmModel.DirectionModel
import io.realm.Realm

class WaysListFragment:Fragment(){
    private lateinit var mAdapter: WaysListAdapter
    @BindView(R.id.rv_root_list)lateinit var mList: RecyclerView
    private lateinit var manager: RecyclerView.LayoutManager
    private val realm = Realm.getDefaultInstance()
    private lateinit var listWays: MutableList<DirectionModel>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_way_list, container, false)
        ButterKnife.bind(this, view)
        manager = LinearLayoutManager(activity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onStart() {
        super.onStart()
        mList.layoutManager = manager
        listWays = realm.where(DirectionModel::class.java).findAll()
        mAdapter = WaysListAdapter(context, listWays)
        mList.adapter = mAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}