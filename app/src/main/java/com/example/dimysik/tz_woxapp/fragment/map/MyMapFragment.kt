package com.example.dimysik.tz_woxapp.fragment.map

import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.AdapterView
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import com.example.dimysik.tz_woxapp.R
import com.example.dimysik.tz_woxapp.api.GeoCodeRepository
import com.example.dimysik.tz_woxapp.api.GeoCodeRepositoryProvider
import com.example.dimysik.tz_woxapp.custom.MyAutoCompleteTextView
import com.example.dimysik.tz_woxapp.model.direction.Example
import com.example.dimysik.tz_woxapp.model.geocode.Result
import com.example.dimysik.tz_woxapp.model.realmModel.DirectionModel
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.maps.GeoApiContext
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.maps.internal.PolylineEncoding
import com.google.android.gms.maps.model.CameraPosition
import io.realm.Realm
import io.realm.RealmList
import io.realm.kotlin.createObject
import java.util.*


class MyMapFragment : Fragment(), OnMapReadyCallback {
    private val TAG: String = MyMapFragment::class.java.simpleName
    //поиск по адресу
    @BindView(R.id.et_start)
    lateinit var mStartWay: MyAutoCompleteTextView
    @BindView(R.id.point_one)
    lateinit var mOnePoint: MyAutoCompleteTextView
    @BindView(R.id.point_two)
    lateinit var mTwoPoint: MyAutoCompleteTextView
    @BindView(R.id.point_three)
    lateinit var mThreePoint: MyAutoCompleteTextView
    @BindView(R.id.point_four)
    lateinit var mFourPoint: MyAutoCompleteTextView
    @BindView(R.id.point_five)
    lateinit var mFivePoint: MyAutoCompleteTextView
    @BindView(R.id.et_finish)
    lateinit var mFinishWay: MyAutoCompleteTextView
    @BindView(R.id.bt_start)
    lateinit var mStart: Button
    @BindView(R.id.bt_add)
    lateinit var mAdd: Button

    //Работа с картой
    private lateinit var gMap: GoogleMap
    //Координаты маркеров
    private val placesMarker: MutableList<LatLng> = mutableListOf()
    private val placesWayPoint: MutableList<String> = mutableListOf()
    private val placesResult: MutableList<String?> = mutableListOf()
    private val placesName: MutableList<String?> = mutableListOf()
    private val placesLat : MutableList<Double> = mutableListOf()
    private val placesLng : MutableList<Double> = mutableListOf()
    //Обработка нажатий
    private val clicked: Clicked = Clicked()

    private val geoCode: GeoCodeRepository = GeoCodeRepositoryProvider.provideGeoCodeRepository()
    private val realm = Realm.getDefaultInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.e(TAG, "start fragment")
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)
        ButterKnife.bind(this, view)
        init()
        return view
    }

    private fun init() {
        mStartWay.addTextChangedListener(null)
        mStartWay.setOnItemClickListener { parent, view, position, id -> setLatLng(mStartWay, parent, position) }
        mOnePoint.addTextChangedListener(null)
        mOnePoint.setOnItemClickListener { parent, view, position, id -> setLatLng(mOnePoint, parent, position) }
        mTwoPoint.addTextChangedListener(null)
        mTwoPoint.setOnItemClickListener { parent, view, position, id -> setLatLng(mTwoPoint, parent, position) }
        mThreePoint.addTextChangedListener(null)
        mThreePoint.setOnItemClickListener { parent, view, position, id -> setLatLng(mThreePoint, parent, position) }
        mFourPoint.addTextChangedListener(null)
        mFourPoint.setOnItemClickListener { parent, view, position, id -> setLatLng(mFourPoint, parent, position) }
        mFivePoint.addTextChangedListener(null)
        mFivePoint.setOnItemClickListener { parent, view, position, id -> setLatLng(mFivePoint, parent, position) }
        mFinishWay.addTextChangedListener(null)
        mFinishWay.setOnItemClickListener { parent, view, position, id -> setLatLng(mFinishWay, parent, position) }

        mStart.setOnClickListener(clicked)
        mAdd.setOnClickListener(clicked)
    }

    private fun setLatLng(view: MyAutoCompleteTextView, parent: AdapterView<*>, position: Int) {
        val results: Result = parent.getItemAtPosition(position) as Result
        when (view.id) {
            R.id.et_start -> {
                placesMarker.add(0,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(0,"${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(0,results.formattedAddress)
                placesLat.add(0,results.geometry!!.location!!.lat!!)
                placesLng.add(0,results.geometry.location!!.lng!!)
            }
            R.id.point_one -> {
                placesMarker.add(2,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(2,"via:${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(2,results.formattedAddress)
                placesLat.add(2,results.geometry!!.location!!.lat!!)
                placesLng.add(2,results.geometry.location!!.lng!!)
            }
            R.id.point_two -> {
                placesMarker.add(3,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(3,"|via:${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(3,results.formattedAddress)
                placesLat.add(3,results.geometry!!.location!!.lat!!)
                placesLng.add(3,results.geometry.location!!.lng!!)
            }
            R.id.point_three -> {
                placesMarker.add(4,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(4,"|via:${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(4,results.formattedAddress)
                placesLat.add(4,results.geometry!!.location!!.lat!!)
                placesLng.add(4,results.geometry.location!!.lng!!)
            }
            R.id.point_four -> {
                placesMarker.add(5,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(5,"|via:${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(5,results.formattedAddress)
                placesLat.add(5,results.geometry!!.location!!.lat!!)
                placesLng.add(5,results.geometry.location!!.lng!!)
            }
            R.id.point_five -> {
                placesMarker.add(6,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(6,"|via:${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(6,results.formattedAddress)
                placesLat.add(6,results.geometry!!.location!!.lat!!)
                placesLng.add(6,results.geometry.location!!.lng!!)
            }
            R.id.et_finish -> {
                placesMarker.add(1,LatLng(results.geometry!!.location!!.lat!!,
                        results.geometry.location!!.lng!!))
                placesWayPoint.add(1,"${results.geometry!!.location!!.lat!!},${results.geometry.location!!.lng!!}")
                placesName.add(1,results.formattedAddress)
                placesLat.add(1,results.geometry!!.location!!.lat!!)
                placesLng.add(1,results.geometry.location!!.lng!!)
            }
        }

        view.setText(results.formattedAddress)
        view.dismissDropDown()
        view.nextFocusDownId
        addMarker()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapView: MapView = view.findViewById(R.id.view_map) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.onResume()

        MapsInitializer.initialize(activity!!.application)

        mapView.getMapAsync(this)
    }

    private lateinit var geoApiContext: GeoApiContext

    override fun onMapReady(googleMap: GoogleMap?) {
        gMap = googleMap!!

        //Получаем контекст для запросов, mapsApiKey хранит в себе String с ключом для карт
        geoApiContext = GeoApiContext.Builder()
                .apiKey(context!!.getString(R.string.google_maps_key))
                .build()
    }

    inner class Clicked : View.OnClickListener {
        override fun onClick(v: View?) {
            if (placesMarker.size >= 2) {
                when (v!!.id) {
                    R.id.bt_start -> addRoute()
                    R.id.bt_add -> addPlace()
                }
            }
        }

    }

    private fun addPlace() {
        when (View.GONE) {
            mOnePoint.visibility -> mOnePoint.visibility = View.VISIBLE
            mTwoPoint.visibility -> mTwoPoint.visibility = View.VISIBLE
            mThreePoint.visibility -> mThreePoint.visibility = View.VISIBLE
            mFourPoint.visibility -> mFourPoint.visibility = View.VISIBLE
            mFivePoint.visibility -> mFivePoint.visibility = View.VISIBLE
        }
    }

    private fun addMarker() {
        val markers = arrayOfNulls<MarkerOptions>(placesMarker.size)
        gMap.clear()
        for (i in 0 until placesMarker.size) {
            markers[i] = MarkerOptions()
                    .position(placesMarker[i])
            gMap.addMarker(markers[i])
        }
        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(placesMarker[placesMarker.size-1], 16.0f)))
    }

    private fun addRoute() {
        if (placesWayPoint.size>=2) {
            Log.e(TAG,"start clicked")
            for (i in 1..placesWayPoint.size) {
                when (i) {
                    1 -> placesResult.add(placesWayPoint[0])
                    2 -> placesResult.add(placesWayPoint[1])
                    3 -> placesResult.add(placesWayPoint[2])
                    else -> placesResult.add(2, "${placesResult[2]}${placesWayPoint[i-1]}")
                }
            }
            if (placesResult.size < 3) placesResult.add(2, null)
            /*compositeDisposable.add(*/geoCode.getRoute(placesResult[0]!!, placesResult[1]!!, placesResult[2])
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ it -> paintRoute(it)}, { it -> Log.e(TAG, it.message, it) })/*)*/
            realm.executeTransaction {
                val model = it.createObject(DirectionModel::class.java/*,UUID.randomUUID()*/)
                for (i in 1 until placesMarker.size) {
                    model.placesLat.add(placesLat[i - 1])
                    model.placesLng.add(placesLng[i - 1])
                    model.placesWayPoint.add(placesWayPoint[i - 1])
                    model.placesName.add(placesName[i - 1])
                }
            }
        }
    }

    private lateinit var marker: Marker
    private var v: Float? = null
    private var lat: Double? = null
    private var lng: Double? = null
    private var handler: Handler? = null
    private var index: Int? = null
    private var next: Int? = null
    private var startPosition: LatLng?=null
    private var endPosition: LatLng?=null
    private var latLngBuilder = LatLngBounds.Builder()
    private var line = PolylineOptions()
    var lineFinal: Polyline? = null
    var latLng: MutableList<com.google.maps.model.LatLng>? = null

    private fun paintRoute(example:Example) {
        gMap.clear()
        if (lineFinal != null){
            lineFinal!!.remove()
        }

        for (i in (0 until example.routes!!.size)) {
            latLng = PolylineEncoding.decode(example.routes[i].overviewPolyline!!.points!!)
            for (j in 0 until latLng!!.size) {
                line.add(LatLng(latLng!![j].lat, latLng!![j].lng))
                latLngBuilder.include(LatLng(latLng!![j].lat, latLng!![j].lng))
            }
        }
        lineFinal = gMap.addPolyline(line)
        val size = resources.displayMetrics.widthPixels
        val latLngBounds = latLngBuilder.build()
        val track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 25)
        gMap.moveCamera(track)

        marker = gMap.addMarker(MarkerOptions().position(placesMarker[0])
                .flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)))

        handler = Handler()
        index = -1
        next = 1

        handler!!.postDelayed(object : Runnable {

            override fun run() {

                if (index!! < latLng!!.size - 1) {
                    index = index!! + 1
                    next = index!! + 1
                }

                if (index!! < latLng!!.size - 1) {
                    startPosition = LatLng(latLng!![index!!].lat, latLng!![index!!].lng)
                    endPosition = LatLng(latLng!![next!!].lat, latLng!![next!!].lng)
                }

                val valueAnimator: ValueAnimator = ValueAnimator.ofFloat(0f, 1f)
                valueAnimator.duration = 3000
                valueAnimator.interpolator = LinearInterpolator()
                valueAnimator.addUpdateListener(ValueAnimator.AnimatorUpdateListener {
                    v = it.animatedFraction

                    lng = v!! * endPosition!!.longitude + (1 - v!!) * startPosition!!.longitude
                    lat = v!! * endPosition!!.latitude + (1 - v!!) * startPosition!!.latitude

                    val newPos: LatLng = LatLng(lat!!, lng!!)
                    marker.position = newPos
                    marker.setAnchor(0.5f, 0.5f)
                    marker.rotation = getBearing(line.points[0], newPos)
                    gMap.moveCamera(CameraUpdateFactory
                            .newCameraPosition
                            (CameraPosition.Builder()
                                    .target(newPos)
                                    .zoom(15.5f)
                                    .build()))
                })
                valueAnimator.start()
                if (index!! < latLng!!.size - 2){
                    handler!!.postDelayed(this, 3000)
                }else if(index!! < latLng!!.size){
                    lineFinal!!.remove()
                    placesLat.clear()
                    placesLng.clear()
                    placesMarker.clear()
                    placesName.clear()
                    placesResult.clear()
                    placesWayPoint.clear()
                }
            }
        }, 0)

    }

    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return -1f
    }

}