package com.example.dimysik.tz_woxapp.api

import com.example.dimysik.tz_woxapp.model.geocode.Example
import io.reactivex.Single
import java.util.*

class GeoCodeRepository(val geoCodeInterface: GeoCodeInterface){
    fun getLocation(address: String):Single<Example>{
        return geoCodeInterface.getLocation(address)
    }

    fun getRoute(origin:String, destination:String, waypoints:String?):Single<com.example.dimysik.tz_woxapp.model.direction.Example>{
        return geoCodeInterface.getRoute(origin,destination,waypoints, "walking")
    }
}